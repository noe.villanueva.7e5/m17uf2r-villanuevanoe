using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<Item> inventory = new List<Item>();
    private float MaxItemsInInventory;

    public void AddItem(Item item)
    {
        //Afegir a la llista d'items.
        inventory.Add(item);
    }

    public void RemoveItem(Item item)
    {
        //Eliminar a la llista d'items.
        inventory.Remove(item);
    }

    public void UseItem(int ItemIndex)
    {
        //Use() de l'item
    }
}

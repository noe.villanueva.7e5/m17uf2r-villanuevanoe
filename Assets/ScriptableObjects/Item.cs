using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewItemData", menuName ="ItemData", order = 1)]
public class Item : ScriptableObject
{
    public string ItemName;
    public Sprite ItemSprite; 
    public float DropProbability;
    public GameObject Prefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Use()
    {
        //Funcio de l'objecte (curar, power-up)
    }
}

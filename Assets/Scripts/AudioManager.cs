using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public AudioMixer audioMixer;
    public AudioSource source;
    public AudioClip normalMusic;
    public AudioClip lowHealthMusic;

    public float lowHealth = 30f;
    public Player player;

    public void SetMusicVolume()
    {
        audioMixer.SetFloat("Music", 0.3f);
    }

    public void SetSFXVolume()
    {
        audioMixer.SetFloat("SFX", 0.5f);
    }
    
    void Start()
    {
        source = source.GetComponent<AudioSource>();
        //player = GetComponent<Player>();

        PlayNormalMusic();
    }

    void Update()
    {
        if (player.currentHealth <= lowHealth)
        {
            PlayUrgentMusic();
        }
        else
        {
            PlayNormalMusic();
        }
    }

    private void PlayNormalMusic()
    {
        if (source.clip != normalMusic)
        {
            source.Stop();
            source.clip = normalMusic;
            source.Play();
        }
    }

    private void PlayUrgentMusic()
    {
        if (source.clip != lowHealthMusic)
        {
            source.Stop();
            source.clip = lowHealthMusic;
            source.Play();
        }
    }
}

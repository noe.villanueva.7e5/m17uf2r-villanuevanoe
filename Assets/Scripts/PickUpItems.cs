using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpItems : MonoBehaviour
{
    private Player player;
    public int healing = 35;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && this.tag == "Coin")
        {
            GameManager.CoinsAmount += 1;
            Destroy(gameObject);
        }
        else if (collision.tag == "Player" && this.tag == "Potion")
        {
            player = collision.GetComponent<Player>();
            player.Heal(healing);
            Destroy(gameObject);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prop : Character
{
    // Start is called before the first frame update
    void Start()
    {
        health = 100;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void TakeDamage(int damage)
    {
        health -= damage;
        if (health <= 0) Die();
    }
    public override void Die()
    {
        GetComponent<DropableItems>().InstantiateLoot(transform.position);
        Destroy(gameObject);
    }
}

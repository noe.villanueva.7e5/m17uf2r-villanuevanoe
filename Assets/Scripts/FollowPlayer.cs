using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public GameObject jugador;
    public Camera CamaraJuego;
    
    void Update()
    {
        if (jugador != null)
        {
            CamaraJuego.transform.position = new Vector3(
                jugador.transform.position.x,
                jugador.transform.position.y,
                CamaraJuego.transform.position.z
                );
        }
    }
}



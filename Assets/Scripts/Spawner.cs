using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    public GameObject enemy;
    public GameObject spawner;
    public GameObject spawner2;
    public GameObject door;
    public SFXSounds sound;
    public AudioClip[] audioClip = new AudioClip[6];
    
    public Vector3 SpawnPoint;
    private int WaveNumber = 1;
    private int EnemiesSpawned = 0;
    private int MaxEnemiesSpawned = 4;
    private int MaxWaves = 5;
    private float contador;
    private float next_spawn_time = 4f;
    private float x, y;

    void Start()
    {
        //next_spawn_time = Time.time + 4f;
        StartCoroutine(Wave1Clip());
    }


    void Update()
    {
        contador += Time.deltaTime;

        if (contador >= next_spawn_time && EnemiesSpawned < MaxEnemiesSpawned)
        {
            Spawn();
            contador = 0f;
            EnemiesSpawned++;
            Debug.Log("E" + EnemiesSpawned);
            Debug.Log("W" + WaveNumber);
        }
        else if (EnemiesSpawned == MaxEnemiesSpawned)
        {
            EnemiesSpawned = 0;
            WaveNumber++;

            switch (WaveNumber)
            {
                case 1:
                    next_spawn_time = 4f;
                    break;
                case 2:
                    sound.GetComponent<SFXSounds>().PlayClip(audioClip[1]);
                    next_spawn_time = 3f;
                    break;
                case 3:
                    sound.GetComponent<SFXSounds>().PlayClip(audioClip[2]);
                    next_spawn_time = 2f;
                    break;
                case 4:
                    sound.GetComponent<SFXSounds>().PlayClip(audioClip[3]);
                    next_spawn_time = 1.5f;
                    break;
                case 5:
                    sound.GetComponent<SFXSounds>().PlayClip(audioClip[4]);
                    next_spawn_time = 1f;
                    break;
                case 6:
                    next_spawn_time = 10f;
                    StartCoroutine(WinClip());
                    break;
            }
        }
        
    }
    private void Spawn()
    {
        //Random position de Spawn
        x = Random.Range(7, -12);
        y = Random.Range(4, -4);
        SpawnPoint.x = x;
        SpawnPoint.y = y;
        Instantiate(enemy, SpawnPoint, Quaternion.identity);
    }

    private void DisableGOs()
    {
        spawner.SetActive(false);
        door.SetActive(false);
        spawner2.SetActive(true);
    }

    IEnumerator Wave1Clip()
    {
        yield return new WaitForSeconds(2);
        sound.GetComponent<SFXSounds>().PlayClip(audioClip[0]);
    }

    IEnumerator WinClip()
    {
        yield return new WaitForSeconds(3);
        sound.GetComponent<SFXSounds>().PlayClip(audioClip[5]);
        DisableGOs();
    }
}


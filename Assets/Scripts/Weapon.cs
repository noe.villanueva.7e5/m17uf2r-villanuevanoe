using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public Transform firePoint;
    public GameObject bulletPrefab;
    private SpriteRenderer spriteRenderer;
    public float WeaponDamage;
    public float WeaponRange;
    public float WeaponAmmo;

    public Camera camera;

    void Start()
    {
        firePoint = transform.parent.GetChild(1);
        camera = Camera.main;
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    // Update is called once per frame
    void Update()
    {
        RotateTowardsMouse();
        if (Input.GetMouseButtonDown(0)) Shoot();
    }

    void RotateTowardsMouse() 
    {
        float angle = GetAngleTowardsMouse();

        transform.rotation = Quaternion.Euler(0, 0, angle);
        spriteRenderer.flipY = angle >= 90 && angle <= 270;
    }

    float GetAngleTowardsMouse()
    {
        Vector3 mouseWorldPosition = camera.ScreenToWorldPoint(Input.mousePosition);
        Vector3 mouseDirection = mouseWorldPosition - transform.position;
        mouseDirection.z = 0;

        float angle = (Vector3.SignedAngle(Vector3.right, mouseDirection, Vector3.forward) + 360) % 360;

        return angle;
    }

    void Shoot()
    {
        GameObject bullet = Instantiate(bulletPrefab);
        bullet.transform.position = firePoint.position;
        bullet.transform.rotation = transform.rotation;
    }

    void Reload()
    {

    }
    
    //Crea una clase "ArmaActual" que contenga una referencia a la arma actual del jugador y un m�todo para cambiar de arma.


}

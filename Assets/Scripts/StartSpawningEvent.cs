using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartSpawningEvent : MonoBehaviour
{
    public GameObject spawner;
    public GameObject doors1;
    public GameObject doors2;
    public GameObject weapon;
    public ShakeCamera shake;

    void Start()
    {
       
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (Input.GetKey(KeyCode.E))
        {
            shake.GetComponent<ShakeCamera>().start = true;
            spawner.SetActive(true);
            doors1.SetActive(true);
            doors2.SetActive(true);
            weapon.SetActive(true);
            Destroy(gameObject);
        }
    }
}

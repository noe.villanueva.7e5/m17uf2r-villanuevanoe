using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : Character
{
    public HealthBar healthBar;

    // Start is called before the first frame update
    void Start()
    {
        health = 500;
        healthBar.SetHealth(health);
    }

    // Update is called once per frame
    void Update()
    {
        healthBar.SetHealth(health);
    }

    public override void TakeDamage(int damage)
    {
        health -= damage;
        if (health <= 0) Die();
    }

    public override void Die()
    {
        //Activar animaci� mort
        GetComponent<DropableItems>().InstantiateLoot(transform.position);
        Destroy(gameObject);
    }
}

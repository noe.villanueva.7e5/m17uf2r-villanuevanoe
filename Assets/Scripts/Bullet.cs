using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float bulletSpeed;
    public int damage;
    public Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = transform.right * bulletSpeed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Enemy enemy = collision.GetComponent<Enemy>();
        Boss boss = collision.GetComponent<Boss>();
        Prop prop = collision.GetComponent<Prop>();

        if (enemy != null)
        {
            enemy.TakeDamage(damage);
            //enemy.audiosource.PlayOneShot(enemy.audioClip);
        }
        if (boss != null)
        {
            boss.TakeDamage(damage);
        }
        if (prop != null)
        {
            prop.TakeDamage(50);
        }

        //Activar animacion explosion
        if (collision.tag != "Player") Destroy(gameObject);
    }
}

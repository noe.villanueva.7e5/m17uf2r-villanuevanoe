using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveLifeBar : MonoBehaviour
{
    private Vector3 origin;
    public GameObject player;
    void Start()
    {
        origin = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = player.transform.position + origin;
    }
}

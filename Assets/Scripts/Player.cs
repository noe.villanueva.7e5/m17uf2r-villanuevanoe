using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : Character
{
    public HealthBar healthBar;
    public int maxHealth = 100;
    public int currentHealth;
    public SFXSounds sound;
    public AudioClip audioClip;
    public AudioClip audioClipDie;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetHealth(currentHealth);
    }

    // Update is called once per frame
    void Update()
    {
        healthBar.SetHealth(currentHealth);
    }

    public override void TakeDamage(int amount)
    {
        currentHealth -= amount;
        sound.GetComponent<SFXSounds>().PlayClip(audioClip);

        if (currentHealth <= 0)
        {
            Die();
        }
    }

    public override void Die()
    {
        //Destroy(this.gameObject);
        sound.GetComponent<SFXSounds>().PlayClip(audioClipDie);
        SceneManager.LoadScene("GameOverScene");
        Cursor.visible = true;
    }

    public void Heal(int vida)
    {
        currentHealth += vida;
        currentHealth = Mathf.Min(currentHealth, maxHealth);
    }
}

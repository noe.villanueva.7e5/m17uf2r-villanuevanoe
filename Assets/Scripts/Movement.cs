using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movement : MonoBehaviour
{
    public float moveHorizontal;
    public float moveVertical; 
    public float speed;
    public Rigidbody2D player;
    private Vector2 movement;
    private Animator animator;
    private Vector3 direction;

    [SerializeField] private float _dashingTime = 0.2f;
    [SerializeField] private float _dashForce = 20f;
    [SerializeField] private float _timeCanDash = 3f;
    private bool _isDashing;
    private bool _canDash = true;

    [SerializeField] private Slider cooldownBar;
    private float currentCooldown = 0f;
    private bool isCooldown = false;

    // Start is called before the first frame update
    void Start()
    {
        speed = 8f;
        animator = GetComponent<Animator>();
        currentCooldown = 0f;
        cooldownBar.maxValue = _timeCanDash;
        cooldownBar.value = _timeCanDash;
    }

    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        if (horizontal != 0 || vertical != 0)
        {
            animator.SetFloat("BlendX", horizontal);
            animator.SetFloat("BlendY", vertical);
            animator.SetFloat("Speed", 1);

            direction = (Vector3.up * vertical + Vector3.right * horizontal).normalized;
            transform.Translate(direction * speed * Time.deltaTime);
        }
        else
        {
            animator.SetFloat("Speed", 0);
        }

        if (Input.GetKeyDown(KeyCode.Space) && _canDash)
        {
            StartCoroutine(Dash());
        }

        if (isCooldown)
        {
            currentCooldown -= Time.deltaTime;

            if (currentCooldown <= 0f)
            {
                currentCooldown = 0f;
                isCooldown = false;
            }

            cooldownBar.value = _timeCanDash - currentCooldown;
        }
    }

    private IEnumerator Dash()
    {
        _isDashing = true;
        _canDash = false;
        player.velocity = new Vector2(direction.x * _dashForce, 0f);
        currentCooldown = _timeCanDash;
        isCooldown = true;

        yield return new WaitForSeconds(_dashingTime);
        _isDashing = false;
        player.velocity = Vector2.zero;
        yield return new WaitForSeconds(_timeCanDash);
        _canDash = true;
    }

  
}

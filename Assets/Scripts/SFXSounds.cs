using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXSounds : MonoBehaviour
{
    public AudioSource audioSource;

    public void PlayClip(AudioClip audioclip)
    {
        audioSource.PlayOneShot(audioclip);
    }
}

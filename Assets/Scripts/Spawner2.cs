using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner2 : MonoBehaviour
{
    [SerializeField]
    public GameObject enemy;
    public GameObject spawner;
    public GameObject door;
    public GameObject door2;
    public SFXSounds sound;
    public AudioClip[] audioClip = new AudioClip[6];
    public List<Vector3> spawnPoints = new List<Vector3>();
    public Vector3 SpawnPoint;
    private int EnemiesSpawned = 0;
    private int MaxEnemiesSpawned = 15;
    private float contador;
    private float next_spawn_time = 5f;

    void Start()
    {
        //StartCoroutine(StartClip());
        spawnPoints.Add(new Vector3(36, -2.9f, 0));
        spawnPoints.Add(new Vector3(45, -2.9f, 0));
        spawnPoints.Add(new Vector3(36, -10.9f, 0));
        spawnPoints.Add(new Vector3(45, -10.9f, 0));
    }


    void Update()
    {
        /*contador += Time.deltaTime;

        if (contador >= next_spawn_time && EnemiesSpawned < MaxEnemiesSpawned)
        {
            Spawn();
            contador = 0f;
            EnemiesSpawned++;
            Debug.Log("E" + EnemiesSpawned);
        }
        else if (EnemiesSpawned == MaxEnemiesSpawned)
        {
            EnemiesSpawned = 0;

            next_spawn_time = 10f;
            StartCoroutine(WinClip());
                   
        }*/
        
    }
    private void Spawn()
    {
        //Random position de Spawn
        int r = Random.Range(1, 4); 
      
        SpawnPoint.x = spawnPoints[r].x;
        SpawnPoint.y = spawnPoints[r].y;
        SpawnPoint.z = spawnPoints[r].z;

        Instantiate(enemy, SpawnPoint, Quaternion.identity);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Me activo rey");
            door.SetActive(true);
            door2.SetActive(true);

            StartCoroutine(SpawnEnemies());
        }
    }

    private IEnumerator SpawnEnemies()
    {
        while (EnemiesSpawned < MaxEnemiesSpawned)
        {
            Spawn();
            EnemiesSpawned++;
            Debug.Log("E" + EnemiesSpawned);

            yield return new WaitForSeconds(next_spawn_time);
        }

        DisableGOs();
        StartCoroutine(WinClip());
    }

    private void DisableGOs()
    {
        spawner.SetActive(false);
        door.SetActive(false);
        door2.SetActive(true);
    }

    IEnumerator StartClip()
    {
        yield return new WaitForSeconds(2);
        sound.GetComponent<SFXSounds>().PlayClip(audioClip[0]);
    }

    IEnumerator WinClip()
    {
        yield return new WaitForSeconds(3);
        sound.GetComponent<SFXSounds>().PlayClip(audioClip[5]);
    }
}


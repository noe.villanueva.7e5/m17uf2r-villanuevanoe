using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropableItems : MonoBehaviour
{
    //public GameObject droppedItemPrefab;
    public List<Item> lootlist = new List<Item>();

    Item GetDroppedItem()
    {
        int randomNumber = Random.Range(1, 100);
        List<Item> possibleItems = new List<Item>();

        foreach (Item item in lootlist)
        {
            if (randomNumber <= item.DropProbability) possibleItems.Add(item);
        }

        if(possibleItems.Count > 0)
        {
            Item droppedItem = possibleItems[Random.Range(0, possibleItems.Count)];
            return droppedItem;
        }

        Debug.Log("No loot dropped");
        return null;
    }

    public void InstantiateLoot(Vector3 spawnPosition)
    {
        Item droppedItem = GetDroppedItem();

        if (droppedItem != null)
        {
            GameObject lootGameObject = Instantiate(droppedItem.Prefab, spawnPosition, Quaternion.identity);
            lootGameObject.GetComponent<SpriteRenderer>().sprite = droppedItem.ItemSprite;
        }
    }
}

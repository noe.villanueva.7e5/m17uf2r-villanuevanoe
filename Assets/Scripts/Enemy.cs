using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character
{
    public HealthBar healthBar;
    public int damageAmount = 10;
    private float collisionCooldown = 1f;
    private bool canDamage = true;
    public SFXSounds sound;
    public AudioClip audioClip;
    public AudioClip audioClipDie;

    // Start is called before the first frame update
    void Start()
    {
        sound = GameObject.Find("AudioMixer").GetComponentInChildren<SFXSounds>();
        health = 100;
        healthBar.SetHealth(health);
    }

    // Update is called once per frame
    void Update()
    {
        healthBar.SetHealth(health);
    }

    public override void TakeDamage(int damage)
    {
        health -= damage;
        
        if (health <= 0)
        { 
            Die();
        }
        else
        {
            sound.GetComponent<SFXSounds>().PlayClip(audioClip);
        }   
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player") && canDamage)
        {
            Player playerHealth = other.gameObject.GetComponent<Player>();
            if (playerHealth != null)
            {
                playerHealth.TakeDamage(damageAmount);
            }

            canDamage = false;
            Invoke("ResetDamageCooldown", collisionCooldown);
        }
    }

    private void ResetDamageCooldown()
    {
        canDamage = true;
    }

    public override void Die()
    {
        //Activar animaci� mort
        GetComponent<DropableItems>().InstantiateLoot(transform.position);
        sound.GetComponent<SFXSounds>().PlayClip(audioClipDie);
        Destroy(gameObject);
        GameManager.KillsAmount += 1;
    }
}

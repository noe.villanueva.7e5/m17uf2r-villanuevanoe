using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public static int CoinsAmount = 0;
    public static int KillsAmount = 0;
    public TextMeshProUGUI textCoins;
    public TextMeshProUGUI textKills;
    public GameObject panel;
    public GameObject panelShop;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        UpdateCoinsText();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateCoinsText();
        UpdateKillsText();
    }

    private void UpdateCoinsText()
    {
        textCoins.text = CoinsAmount.ToString();
    }
    private void UpdateKillsText()
    {
        textKills.text = KillsAmount.ToString();
    }
    public void StartGame()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene("MenuScene");
    }

    public void ActivateOptionsMenu()
    {
        panel.SetActive(true);
    }

    public void ActivateShop()
    {
        panelShop.SetActive(true);
    }

    public void DesactivateOptionsMenu()
    {
        panel.SetActive(false);
        panelShop.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
    public void RemoveCoins(int amount)
    {
        CoinsAmount -= amount;
    }
}

using UnityEngine;
using UnityEngine.UI;

public class ShopItem : MonoBehaviour
{
    public Product product;
    public Text nameLabel;
    public Text priceLabel;

    private Button buyButton;

    private void Start()
    {
        nameLabel.text = product.name;
        priceLabel.text = product.price.ToString();

        buyButton = GetComponentInChildren<Button>();
        buyButton.onClick.AddListener(Buy);
    }

    private void Buy()
    {
        if (GameManager.CoinsAmount >= product.price)
        {
            GameManager.Instance.RemoveCoins(product.price);
        }
    }
}
